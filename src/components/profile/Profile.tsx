import React from 'react';
import { IonAvatar, IonBackButton, IonButton, IonButtons, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonInput, IonItem, IonItemGroup, IonLabel, IonListHeader, IonPage, IonRow, IonTextarea, IonTitle, IonToolbar } from '@ionic/react';
import devGabriel from '../../assets/img/dev-gabriel.png';
import { logOutOutline, informationCircleOutline, business, starOutline, thumbsUp, helpCircleOutline} from 'ionicons/icons';
import './Profile.css';

const Profile: React.FC = () => {
    const meuApelido: string = "Gabs";
    const minhaBio: string = "19 anos. Universitário, Programador, gosto de Indie e uma boa conversa."

    return (
        <IonPage className="profile_page">
            <IonHeader>
                <IonToolbar mode="ios" className="custom_toolbar">
                    <IonButtons slot="start">
                        <IonBackButton defaultHref="/home" text="" color="light"></IonBackButton>
                    </IonButtons>
                    <IonButtons slot="end">
                        <IonButton href="/welcome" mode="ios" color="light" fill="clear">
                            Sair
                            <IonIcon className="ion-margin-left" color="light" icon={logOutOutline} />
                        </IonButton>
                    </IonButtons>
                    <IonTitle>Perfil</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                <IonGrid>
                    <IonRow>
                        <IonCol className="avatar_container">
                            <IonAvatar >
                                <img src={devGabriel} />
                            </IonAvatar>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol>
                            <IonItemGroup>
                                <IonListHeader className="editinfo_header">Editar Informações</IonListHeader>
                                <IonItem>
                                    <IonLabel position="stacked">Apelido</IonLabel>
                                    <IonInput type="text">{meuApelido}</IonInput>
                                </IonItem>
                                <IonItem>
                                    <IonLabel position="stacked">
                                        Bio
                                    </IonLabel>
                                    <IonTextarea autoGrow>
                                        {minhaBio}
                                    </IonTextarea>
                                </IonItem>
                                <IonItem lines="full" mode="ios" button>
                                    <IonIcon size="small" slot="start" icon={informationCircleOutline} />
                                    Editar Informações Pessoais
                                </IonItem>
                                <IonItem lines="full" mode="ios" button>
                                    <IonIcon size="small" slot="start" icon={business} />
                                    Editar Endereço
                                </IonItem>
                                <IonItem lines="full" mode="ios" button>
                                    <IonIcon size="small" slot="start" icon={starOutline} />
                                    Planos
                                </IonItem>
                                <IonItem lines="full" mode="ios" button>
                                    <IonIcon size="small" slot="start" icon={thumbsUp} />
                                    Avalie-nos
                                </IonItem>
                                <IonItem href="/sobre" lines="full" mode="ios" button>
                                    <IonIcon size="small" slot="start" icon={helpCircleOutline} />
                                    Sobre
                                </IonItem>
                            </IonItemGroup>
                            <div className="ion-margin-vertical ion-padding-top">
                                <IonButton className="save_button" size="large" expand="block" mode="ios" fill="solid"><b>Salvar</b></IonButton>
                            </div>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonPage>
    )
}

export default Profile
