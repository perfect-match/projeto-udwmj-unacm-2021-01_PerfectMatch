import { IonCard, IonCardContent, IonCardHeader, IonTitle } from '@ionic/react'
import React from 'react'
import { UserItem } from '../../models/UserItem'

const UserDetail: React.FC<{ user: UserItem }> = ({ user }) => {
    return (
        <IonCard style={{ minHeight: '70vh' }} button>
            <IonCardHeader>
                <IonTitle color="dark">
                    Informações Adicionais
                </IonTitle>
            </IonCardHeader>
            <IonCardContent className="ion-text-center">
                Otras informçaões sobre {user.name}
            </IonCardContent>
        </IonCard>
    )
}

export default UserDetail
