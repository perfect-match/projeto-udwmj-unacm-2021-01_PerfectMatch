import React from 'react'
import { IonAvatar, IonBackButton, IonButtons, IonContent, IonHeader, IonIcon, IonInput, IonItem, IonLabel, IonPage, IonTitle, IonToolbar } from '@ionic/react'
import brunoHenrique from '../../assets/img/bruno-henrique.jpg';
import marcelaNogueira from '../../assets/img/marcela-nogueira.jpg';
import ninaAlmeida from '../../assets/img/nina-almeida.jpg';
import './Chats.css';

interface ChatItem {
    userPic: string;
    userName: string;
}

const conversas: ChatItem[] = [
    {
        userPic: brunoHenrique,
        userName: 'Bruno Henrique',
    },
    {
        userPic: marcelaNogueira,
        userName: 'Marcela Nogueira',
    },
    {
        userPic: ninaAlmeida,
        userName: 'Nina Almeida',
    }
]

const ChatItemComponent: React.FC<{user: ChatItem}> = ({user}) => {
    return (
        <IonItem className="ion-marginp-top" mode="md" button>
            <IonAvatar slot="start">
                <img src={user.userPic} />
            </IonAvatar>
            <IonLabel>{user.userName}</IonLabel>
            <IonIcon slot="end" />
        </IonItem>
    )
}

const Chats: React.FC = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar mode="ios" className="custom_toolbar">
                    <IonTitle>Conversas</IonTitle>
                    <IonButtons slot="start">
                        <IonBackButton color="light" defaultHref="/home" text="" />
                    </IonButtons>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                {
                    conversas.map((user, i) => {
                        return <ChatItemComponent key={`user--${i}`} user={user}/>
                    })
                }
                <div className="div_info ion-text-center">
                    <IonLabel>Clique para abrir uma conversa</IonLabel>
                </div>
            </IonContent>

        </IonPage>
    )
}

export default Chats
