import React, { ReactElement } from 'react';
import ReactCardFlip from 'react-card-flip';

const FlipCard: React.FC<{ frontside?: ReactElement<any>, backside?: ReactElement<any> }> = ({ frontside, backside }) => {

  const [isFlipped, setIsFlipped] = React.useState(false);

  const handleClick = () => setIsFlipped(prev => !prev)

  return (
    <ReactCardFlip isFlipped={isFlipped} flipDirection="horizontal">
      <div onClick={handleClick}>
        {frontside}
      </div>

      <div onClick={handleClick}>
        {backside}
      </div>
    </ReactCardFlip>
  )
}

export default FlipCard;