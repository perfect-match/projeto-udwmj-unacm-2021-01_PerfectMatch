import React from 'react'
import { UserItem } from '../../models/UserItem';
import { IonCard, IonImg, IonCardContent, IonButton, IonIcon } from '@ionic/react';
import { closeCircle, checkmarkCircle } from 'ionicons/icons';

const Card: React.FC<{ user: UserItem }> = ({ user }) => {
    return (
        <IonCard style={{ height: '70vh' }} button>
            <IonImg src={user.imgSrc} />
            <IonCardContent>
                <h1>{user.name}</h1>
                <p>{user.age} anos. {user.bio}</p>
                <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <div>
                        <p><small>Compatibilidade: <b>{user.compatibility}%</b></small></p>
                    </div>
                    <div>
                        <small><b>{user.distance}km</b></small>
                    </div>
                </div>
            </IonCardContent>
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', height: '3rem' }}>
                <div>
                    <IonButton fill="clear" size="large" color="danger">
                        <IonIcon size="large" icon={closeCircle} />
                    </IonButton>
                </div>
                <div>
                    <IonButton fill="clear" size="large" color="success">
                        <IonIcon size="large" icon={checkmarkCircle} />
                    </IonButton>
                </div>
            </div>
            <div className="ion-text-center">
                <p><small><em>clique para saber mais</em></small></p>
            </div>
        </IonCard>
    )
};

export default Card;
