import React from 'react'
import { IonBackButton, IonButtons, IonContent, IonHeader, IonListHeader, IonPage, IonSearchbar, IonTitle, IonToolbar } from '@ionic/react'
import dude1 from '../../assets/img/dude1.jpg'
import chic1 from '../../assets/img/chic1.jpg'
import dude2 from '../../assets/img/dude2.jpg'
import chic2 from '../../assets/img/chic2.jpg'
import { UserItem } from '../../models/UserItem'
import Card from '../card/CardContent'
import FlipCard from '../card/CardFlipper'
import UserDetail from '../user-detail/UserDetail'

const Users: UserItem[] = [
    {
        imgSrc: dude1,
        age: 29,
        name: 'Mateus Gonçalves',
        bio: 'Engenheiro Civil, gosto de praticar caminhadas em montanha e curto musica Pop.',
        distance: '2.8',
        compatibility: 78,
    },
    {
        imgSrc: chic1,
        age: 23,
        name: 'Natalia Rodrigues',
        bio: 'Sou canceriana e estou a procura de um leonino, não fumo e gosto de Musica sertanejo',
        distance: '4.0',
        compatibility: 88,
    },
    {
        imgSrc: chic2,
        age: 19,
        name: 'Amanda Freitas',
        bio: 'Amante de gatos, Gosto de festas e um bom drink.',
        distance: '0.2',
        compatibility: 90,
    },
    {
        imgSrc: dude2,
        age: 21,
        name: 'Henrique Silva',
        bio: ' Universitario cursando Ciencia da computação, gosto de jogos de FPS e sou Patente Imortal II no valorant.',
        distance: '0.7',
        compatibility: 85,
    }
]

const Explore: React.FC = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar className="custom_toolbar" mode="ios">
                    <IonTitle>Explorar</IonTitle>
                    <IonButtons slot="start">
                        <IonBackButton defaultHref="/home" color="light" text=""></IonBackButton>
                    </IonButtons>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                <IonToolbar mode="ios" className="ion-margin-top">
                    <IonSearchbar placeholder="ex.: Nome, idade, bairro " />
                </IonToolbar>
                <IonListHeader>
                    <h5><b>Sugeridos para você</b></h5>
                </IonListHeader>
                {
                    Users.map((user, i) => {
                        return <FlipCard
                            key={`user--${i}`}
                            frontside={<Card user={user}></Card>}
                            backside={<UserDetail user={user}></UserDetail>}
                        />
                    })
                }
            </IonContent>
        </IonPage>
    )
}

export default Explore
