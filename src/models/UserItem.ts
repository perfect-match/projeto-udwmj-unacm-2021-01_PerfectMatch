export interface UserItem {
    imgSrc?: string;
    age: number;
    name: string;
    bio: string;
    distance: string;
    compatibility: number;
}