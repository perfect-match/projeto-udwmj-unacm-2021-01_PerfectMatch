import React from 'react'
import { IonButton, IonContent, IonPage, IonImg } from '@ionic/react'
import gayCouple from '../../assets/img/gay-couple.png';
import './Welcome.css';

const Welcome: React.FC = () => {

    return (
        <IonPage>
            <IonContent fullscreen>
                <div style={{ height: '100vh', display: 'flex', flexDirection: 'column' }} className="ion-justify-content-between">
                    <div className="header ion-margin-horizontal ion-text-center">
                        <h1><strong>Perfect Match</strong></h1>
                    </div>
                    <IonImg className="ion-margin-bottom gay-couple" src={gayCouple}></IonImg>
                    <div className="ion-margin-horizontal ion-text-center">
                        <p><strong>Chega de fazer coleção de Matchs, venha encontrar o seu par perfeito!</strong></p>
                    </div>
                    <div className="ion-margin">
                        <IonButton
                            size="large"
                            mode="ios"
                            fill="solid"
                            expand="block"
                            routerLink="/login"
                            routerDirection="forward"
                            className="login ion-margin-bottom">
                            Login
                        </IonButton>
                        <IonButton
                            size="large"
                            mode="ios"
                            fill="solid"
                            expand="block"
                            routerLink="/cadastro"
                            routerDirection="forward"
                            className="singin">
                            Cadastrar-se
                        </IonButton>
                    </div>
                </div>
            </IonContent>
        </IonPage >
    )
}

export default Welcome
