import React from 'react'
import { IonLabel, IonItem, IonList, IonSelect, IonSelectOption, IonHeader, IonTextarea, IonListHeader, IonInput } from '@ionic/react'
import './Register.css';

const Preferences: React.FC = () => {
    return (
        <div className="ion-margin-vertical">
            <IonItem>
                <IonLabel position="floating">Apelido</IonLabel>
                <IonInput type='text'></IonInput>
            </IonItem>
            <IonItem>
                <IonLabel>Interesses</IonLabel>
                <IonSelect slot='end'>
                    <IonSelectOption value="">No sexo oposto</IonSelectOption>
                    <IonSelectOption value="not-much">No mesmo sexo</IonSelectOption>
                    <IonSelectOption value="normal">Em ambos os sexos</IonSelectOption>
                </IonSelect>
            </IonItem>
            <IonItem className="ion-margin-bottom">
                <IonLabel position="floating">Bio</IonLabel>
                <IonTextarea autoGrow wrap="hard" mode="md" placeholder="Fale um pouco sobre você aqui"></IonTextarea>
            </IonItem>

            <IonListHeader>
                <h3><b>O que eu acho sobre...</b></h3>
            </IonListHeader>
            <IonItem>
                <IonLabel>Relacionamento</IonLabel>
                <IonSelect slot='end'>
                    <IonSelectOption value="none">Nenhuma</IonSelectOption>
                    <IonSelectOption value="not-much">Não muito</IonSelectOption>
                    <IonSelectOption value="normal">Razoavél</IonSelectOption>
                    <IonSelectOption value="some">Considerável</IonSelectOption>
                    <IonSelectOption value="a-lot">Muita</IonSelectOption>
                </IonSelect>
            </IonItem>
            <IonItem>
                <IonLabel>Música</IonLabel>
                <IonSelect slot='end'>
                    <IonSelectOption value="none">Nenhuma</IonSelectOption>
                    <IonSelectOption value="not-much">Não muito</IonSelectOption>
                    <IonSelectOption value="normal">Razoavél</IonSelectOption>
                    <IonSelectOption value="some">Considerável</IonSelectOption>
                    <IonSelectOption value="a-lot">Muita</IonSelectOption>
                </IonSelect>
            </IonItem>
            <IonItem>
                <IonLabel>Hobbies</IonLabel>
                <IonSelect slot='end'>
                    <IonSelectOption value="none">Nenhuma</IonSelectOption>
                    <IonSelectOption value="not-much">Não muito</IonSelectOption>
                    <IonSelectOption value="normal">Razoavél</IonSelectOption>
                    <IonSelectOption value="some">Considerável</IonSelectOption>
                    <IonSelectOption value="a-lot">Muita</IonSelectOption>
                </IonSelect>
            </IonItem>
            <IonItem>
                <IonLabel>Filhos</IonLabel>
                <IonSelect slot='end'>
                    <IonSelectOption value="none">Nenhuma</IonSelectOption>
                    <IonSelectOption value="not-much">Não muito</IonSelectOption>
                    <IonSelectOption value="normal">Razoavél</IonSelectOption>
                    <IonSelectOption value="some">Considerável</IonSelectOption>
                    <IonSelectOption value="a-lot">Muita</IonSelectOption>
                </IonSelect>
            </IonItem>
            <IonItem>
                <IonLabel>Política</IonLabel>
                <IonSelect slot='end'>
                    <IonSelectOption value="none">Nenhuma</IonSelectOption>
                    <IonSelectOption value="not-much">Não muito</IonSelectOption>
                    <IonSelectOption value="normal">Razoavél</IonSelectOption>
                    <IonSelectOption value="some">Considerável</IonSelectOption>
                    <IonSelectOption value="a-lot">Muita</IonSelectOption>
                </IonSelect>
            </IonItem>
        </div>
    )
}

export default Preferences;