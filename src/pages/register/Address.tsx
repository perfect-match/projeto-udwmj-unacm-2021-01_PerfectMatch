import { IonHeader, IonInput, IonItem, IonLabel, IonList } from '@ionic/react'
import React from 'react'

const Address: React.FC = () => {
    return (
        <div className="ion-margin-vertical">
            <IonItem className="ion-margin-top">
                <IonLabel position="floating">CEP</IonLabel>
                <IonInput type="text" />
            </IonItem>
            <IonItem className="ion-margin-top">
                <IonLabel position="floating">Endereço</IonLabel>
                <IonInput type="text" />
            </IonItem>
            <IonItem className="ion-margin-top">
                <IonLabel position="floating">Número</IonLabel>
                <IonInput type="text" />
            </IonItem>
            <IonItem className="ion-margin-top">
                <IonLabel position="floating">Bairro</IonLabel>
                <IonInput type="text" />
            </IonItem>
            <IonItem className="ion-margin-top">
                <IonLabel position="floating">Cidade</IonLabel>
                <IonInput type="text" />
            </IonItem>
            <IonItem className="ion-margin-top">
                <IonLabel position="floating">Estado</IonLabel>
                <IonInput type="text" />
            </IonItem>
        </div>
    )
}

export default Address;