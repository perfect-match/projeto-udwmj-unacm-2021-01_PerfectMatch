import React from 'react'
import { IonList, IonItem, IonLabel, IonInput, IonButton, IonItemGroup, IonHeader } from '@ionic/react'

const PersonalInfo: React.FC = () => {

    return (
        <div className="ion-margin-vertical">
            <IonItemGroup>
                <IonItem className="ion-margin-top">
                    <IonLabel position="floating">Nome Completo</IonLabel>
                    <IonInput type="text" />
                </IonItem>
                <IonItem className="ion-margin-top">
                    <IonLabel position="floating">Email</IonLabel>
                    <IonInput type="email" />
                </IonItem>
                <IonItem className="ion-margin-top">
                    <IonLabel position="floating">Senha</IonLabel>
                    <IonInput type="password" />
                </IonItem>
                <IonItem className="ion-margin-top">
                    <IonLabel position="floating">Repetir a senha</IonLabel>
                    <IonInput type="password" /> <IonButton slot="icon-only" mode="ios"></IonButton>
                </IonItem>
                <IonItem className="ion-margin-top">
                    <IonLabel position="stacked">Data de Nascimento</IonLabel>
                    <IonInput type="date" />
                </IonItem>
                <IonItem className="ion-margin-top">
                    <IonLabel position="stacked">Documento</IonLabel>
                    <IonInput type="text" placeholder="CPF/RG" />
                </IonItem>
            </IonItemGroup>
        </div>
    )
}

export default PersonalInfo;