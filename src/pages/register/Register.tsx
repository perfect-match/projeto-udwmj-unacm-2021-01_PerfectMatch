import React from 'react'
import { IonBackButton, IonButton, IonButtons, IonContent, IonHeader, IonIcon, IonList, IonListHeader, IonPage, IonSegment, IonSegmentButton, IonTitle, IonToolbar } from '@ionic/react'
import { informationCircleOutline, businessOutline, list } from 'ionicons/icons';
import Address from './Address';
import PersonalInfo from './PersonalInfo';
import Preferences from './Preferences';

const Register: React.FC = () => {

    const [currentStep, setCurrentStep] = React.useState(1);

    let text: string = 'p';

    function changeText() {
        switch (currentStep) {
            case 1:
                text = 'Informações Pessoais';
                return text;
            case 2:
                text = 'Endereço';
                return text;
            case 3:
                text = 'Preferências';
                return text;
        }
        console.log('text', text);
    }

    function handleTabValue(): void {
        if (currentStep > 0 && currentStep < 3) {
            setCurrentStep(currentStep + 1);
        }
    }

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar className="custom_toolbar" mode="ios">
                    <IonButtons slot="start">
                        <IonBackButton color="light" defaultHref="/welcome" text="Voltar" />
                    </IonButtons>
                    <IonTitle>Cadastro</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                <IonToolbar>
                    <IonSegment mode="md" value={(currentStep).toString()}>
                        <IonSegmentButton onClick={() => setCurrentStep(1)} value="1">
                            <IonIcon icon={informationCircleOutline} />
                        </IonSegmentButton>
                        <IonSegmentButton onClick={() => setCurrentStep(2)} value="2">
                            <IonIcon icon={businessOutline} />
                        </IonSegmentButton>
                        <IonSegmentButton onClick={() => setCurrentStep(3)} value="3">
                            <IonIcon icon={list} />
                        </IonSegmentButton>
                    </IonSegment>
                </IonToolbar>
                <div className="ion-padding-right">
                    <IonList lines="inset" mode="ios">
                        <IonListHeader>
                            <h3><b>{changeText()}</b></h3>
                        </IonListHeader>

                        {currentStep === 1 && <PersonalInfo />}
                        {currentStep === 2 && <Address />}
                        {currentStep === 3 && <Preferences />}
                    </IonList>
                </div>
                <div className="ion-margin">
                    <IonButton
                        hidden={currentStep === 3}
                        mode="ios"
                        size="large"
                        fill="solid"
                        expand="block"
                        className="register_custom_button"
                        onClick={handleTabValue}>
                        Próximo
                    </IonButton>
                    <IonButton
                        hidden={currentStep !== 3}
                        mode="ios"
                        size="large"
                        fill="solid"
                        expand="block"
                        routerDirection="forward"
                        routerLink="/home"
                        className="register_custom_button"
                        onClick={handleTabValue}>
                        Cadastrar
                    </IonButton>
                </div>
            </IonContent>
        </IonPage>
    )
};

export default Register;
