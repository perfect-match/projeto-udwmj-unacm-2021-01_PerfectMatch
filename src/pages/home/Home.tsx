import React from 'react';
import { Route } from 'react-router';
import { IonReactRouter } from '@ionic/react-router';
import { IonApp, IonContent, IonHeader, IonIcon, IonImg, IonLabel, IonPage, IonRouterOutlet, IonTabBar, IonTabButton, IonTabs, IonTitle, IonToolbar } from '@ionic/react';
import { heartCircle, chatbubblesOutline, personCircleOutline, } from 'ionicons/icons';
import homeIllustration from '../../assets/img/home-illustration.png';
import Profile from '../../components/profile/Profile';
import './Home.css';
import Explore from '../../components/explore/Explore';
import Chats from '../../components/chats/Chats';

const Home: React.FC = () => {
    return (
        <IonPage>
            <IonApp>
                <IonReactRouter>
                    <IonContent>
                        <IonHeader>
                            <IonToolbar mode="ios">
                                <IonTitle>Home</IonTitle>
                            </IonToolbar>
                        </IonHeader>
                        <IonTabs >
                            <IonRouterOutlet>
                                <Route path="/home/explorar" component={Explore} />
                                <Route path="/home/perfil" component={Profile} />
                                <Route path="/home/chats" component={Chats} />
                            </IonRouterOutlet>
                            <IonTabBar slot="bottom">
                                <IonTabButton tab="/explorar" href="/home/explorar">
                                    <IonLabel>Explorar</IonLabel>
                                    <IonIcon icon={heartCircle} />
                                </IonTabButton>
                                <IonTabButton tab="/chats" href="/home/chats">
                                    <IonLabel>Chats</IonLabel>
                                    <IonIcon icon={chatbubblesOutline}></IonIcon>
                                </IonTabButton>
                                <IonTabButton tab="/perfil" href="/home/perfil">
                                    <IonLabel>Perfil</IonLabel>
                                    <IonIcon icon={personCircleOutline}></IonIcon>
                                </IonTabButton>
                            </IonTabBar>
                        </IonTabs>

                        <IonImg src={homeIllustration} />
                        <div className="container ion-text-center">
                            <h3><b>Vamos começar?</b></h3>
                            <p className="ion-margin-horizontal ion-padding"> Clique em uma aba e comece a busca pelo seu par perfeito!</p>
                        </div>
                    </IonContent>
                </IonReactRouter>
            </IonApp>
        </IonPage>
    );
};

export default Home;
