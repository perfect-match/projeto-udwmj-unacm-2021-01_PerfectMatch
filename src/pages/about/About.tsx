import React from 'react'
import { IonPage, IonContent, IonBackButton, IonButtons, IonHeader, IonToolbar, IonTitle, IonImg, IonFooter } from '@ionic/react'
import iconLogo from '../../assets/img/icon-logo.png';
import cupido from '../../assets/img/cupido.png';

const About: React.FC = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar className="custom_toolbar" mode="ios">
                    <IonTitle>Sobre</IonTitle>
                    <IonButtons>
                        <IonBackButton color="light" defaultHref="/home" text="Voltar"></IonBackButton>
                    </IonButtons>
                </IonToolbar>
            </IonHeader>
            <IonContent className="ion-text-center" fullscreen>
                <div style={{ display: 'flex', flexDirection: 'column' }} className="ion-justify-content-between">
                    <IonImg className='ion-margin-top' style={{ maxWidth: '30vw', marginLeft: 'auto', marginRight: 'auto' }} src={iconLogo} />
                    <div style={{ fontWeight: 'bold', color: '#333' }} className="ion-margin-horizontal">
                        <h1 style={{ fontWeight: 'bold' }} >Sobre Nós</h1>
                        <p>
                            Ao decorrer da pandemia de COVID-19 problemas foram gerados e agravados em toda a humanidade, um deles foi a solidão, o qual nós viemos para combater!
                        </p>
                        <p>
                            Não como mais um app de coleção de Matchs comuns, ou só mais um app para conversar com 10 gostar de 5 e não sair com nenhum, o Perfect Match veio para trazer de uma vez por todas o SEU PAR IDEAL!
                        </p>
                        <p>
                            Filtrando com carinho seus gostos e preferencias mais íntimos e variados procurando cada vez mais achar a sua “metade da laranja”.
                        </p>
                    </div>
                    <IonImg className='ion-margin-top' style={{ maxWidth: '70vw', marginLeft: 'auto', marginRight: 'auto' }} src={cupido} />
                </div>
            </IonContent>
            <IonFooter>
                <IonToolbar style={{ color: '#333' }} mode="ios">
                    <IonTitle>grupo15©</IonTitle>
                </IonToolbar>
            </IonFooter>
        </IonPage>
    )
}

export default About
