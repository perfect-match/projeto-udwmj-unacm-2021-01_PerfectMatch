import React from 'react';
import './Login.css';
import loginIllustration from '../../assets/img/login-illustration.png';
import { IonBackButton, IonButton, IonButtons, IonCol, IonContent, IonFooter, IonGrid, IonHeader, IonImg, IonInput, IonItem, IonLabel, IonListHeader, IonPage, IonRow, IonText, IonToolbar } from '@ionic/react';

const Login: React.FC = () => {
    return (
        <IonPage>
            <IonContent fullscreen>
                <div style={{ height: '100vh', display: 'flex', flexDirection: 'column'}} className="ion-justify-content-between">

                    <IonToolbar mode="ios">
                        <IonButtons slot="start">
                            <IonBackButton defaultHref="/welcome" text="Voltar" />
                        </IonButtons>
                    </IonToolbar>
                    <IonImg src={loginIllustration} className="login-illustration"></IonImg>
                    <div className="ion-margin">
                        <IonListHeader mode="ios">Login</IonListHeader>
                        <IonItem lines="inset">
                            <IonLabel position="floating">
                                Email
                            </IonLabel>
                            <IonInput type="email" />
                        </IonItem>
                        <IonItem lines="inset">
                            <IonLabel position="floating">
                                Senha
                            </IonLabel>
                            <IonInput type="password" />
                        </IonItem>
                        <div className="
                            ion-text-center
                            ion-margin-horizontal">
                            <IonButton fill="clear" mode="ios" color="primary">
                                Esqueci minha senha
                            </IonButton>
                        </div>
                        <div className="ion-margin-vertical">
                            <IonButton
                                mode="ios"
                                size="large"
                                fill="solid"
                                expand="block"
                                className="entrar"
                                routerLink="/home"
                                routerDirection="forward">
                                Entrar
                            </IonButton>
                        </div>
                    </div>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default Login
