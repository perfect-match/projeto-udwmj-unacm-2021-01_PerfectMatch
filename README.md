# Perfect Match - Mobile (front end only)
## Tecnologias Utilizadas
* Ionic
* Capacitor
* ReactJs
## Pré Requisitos
* [Node Js](https://nodejs.org/en/)
## Executando o Projeto
* De clone nesse repositório em uma pasta de sua preferência.
* Instale as dependências do projeto rodando comando `npm install`
* Certifique-se que as dependências foram instaladas e a pasta
node modules foi criada.
* Para Rodar o projeto basta utilizar o comando `npm start`
* Abra seu Browser [http://localhost:3000](http://localhost:3000) para visualizar o projeto.
## Sobre Ionic com React
Para aprender mais sobre a ferramenta [Documentacao Ionic/React](https://ionicframework.com/react#start)